unit slbFunctions;
	interface
			
			uses
				Classes, SysUtils, constants;
			
			type
				TBaseFunctor = function(actor: TL2Live): boolean;
				TCharFunctor = function(actor: TL2Char): boolean;
				TPartyFind = Array of TL2Live;
				
			var
				vLastSkill: integer;
				vLastSkillTime : integer;
				vNotBuffCount: integer = 0;
				partyFindArray: array of TL2Live;
				currentTarget: TL2Live;
				previousTarget: TL2Live;
				fearCastEndTime: Cardinal;
				isFeared: boolean;
				
			{should checkers}
			function shouldProtect(enemy, protectChar: TL2Live): boolean;
			function shouldCast(obj: TL2Live; hp: Cardinal; range: Cardinal; isAttackable: boolean = true): boolean;
			function isTargetChanged(enemy, protectChar: TL2Live): boolean;
			function isFearCasting(enemy: TL2Live): boolean;
			function isCasting: boolean;
			{etc}
			function isPvpStatus: boolean;
			procedure PvPStuff;
			procedure targetSaver;
			procedure partyProtectThread;
			procedure antiFear;
			procedure fearBuffsDispel;
			procedure buffDispel(buffID: Cardinal);
			procedure newDispel;
			procedure antiBackstab;
			procedure medusedTargetAvoid;
			procedure holdTargetOly;
			procedure antiHate;
			{math functions}
			function randomDispersion(x: integer): integer;
			function isContains(arr: array of integer; val: integer): boolean;
			
			{character status}
			function isStunned(obj: TL2Live): boolean;
			function isTranced(obj: TL2Live): boolean;
			function isAffraid(obj: TL2Live): boolean;
			function isMedused(obj: TL2Live): boolean;
			function isAnchored(obj: TL2Live): boolean;
			function isRooted(obj: TL2Live): boolean;
			function isDeathMark(obj: TL2Live): boolean;
			function isSilenced(obj: TL2Live): boolean;
			function isRealTarget(obj: TL2Live): boolean;
			function isUltimateDefense(obj: TL2Live): boolean;
			function isShadowed(obj: TL2Live): boolean;
			//effs
			function isInvincible(obj: TL2Live): boolean;
			function isPvPdebuff(obj: TL2Live): boolean;
			function isSealOfLimit(obj: TL2Live): boolean;
			function isDisabled(obj: TL2Live): boolean;
			function isOverweight(limit: integer): boolean;
			function fNotBuff(sName: string): boolean;
			function fBuffCount: integer;
			procedure fNotBuffCount;
			//function isCasting(obj: TL2Live): boolean;
			//relations
			function isFriend(victim: TL2Live): boolean;
			function isClanMember(victim: TL2Live): boolean;
			function isAllyMember(victim: TL2Live): boolean;
			
			{class defination}
			
			{buffs status}
			
			{class status}
			
	implementation
	
			{should checkers}
			function isCasting;
			begin
				if (User.Cast.EndTime > 0) and 
				((User.Cast.ID = SoulOfPain) or 	{sop}
				(User.Cast.ID = AnnihilationCircle) or	{annihilation circle}
				(User.Cast.ID = CurseOfDivinity))		{curse of divinity}
				then
					isCasting := true
				else
					isCasting := false;
			end;
			
			function isTargetChanged;
			begin
				if not (enemy.Target = protectChar) then
					isTargetChanged := true
				else
					isTargetChanged := false;
			end;
			
			function shouldProtect; {anti fear protect}
			begin
				if (enemy.Target = protectChar) then
				begin
					if (
					(enemy.Cast.EndTime > 0) and (enemy.Cast.ID = (1169))) {fear}			
					then				
						shouldProtect := true
					else
						shouldProtect := false
					end
				else
				shouldProtect := false;
			end;
			
			function ShouldCast;
			var
				buffsTemp: TL2Effect;
			begin
				if 
				not User.Dead 
				and (User.HP > hp) 
				and not obj.Dead
				and not isSilenced(User) 
				and not isDisabled(User)
				and not isInvincible(User)
				and (User.Cast.EndTime = 0)
				and (User.DistTo(obj) < range) then 
				begin
					if isAttackable and obj.Attackable then
						ShouldCast := true
					else if isAttackable and not obj.Attackable then
						ShouldCast := false
					else if not isAttackable then
						ShouldCast := true;
				end
				else
					ShouldCast := false	
			end;
			
			function isFearCasting;
			begin
				if (enemy.Cast.EndTime > 0) and (enemy.Target = User) then
				begin
					if (enemy.Cast.ID = 1169) then
						begin
							fearCastEndTime := enemy.Cast.EndTime;
							isFearCasting := true
						end	
					else
						isFearCasting := false
					end
				else
				isFearCasting := false;
			end;
			
			{etc}
			function isPvpStatus;
			const
				enemySearchRadius = 2000;
			var
				i: integer;
				perlimResult: integer;
			begin
				perlimResult := 0;
				for i:=0 to Charlist.count-1 do
				begin
					if Charlist.Items(i).Dead and (User.DistTo(Charlist.Items(i)) < enemySearchRadius)
					and not isFriend(Charlist.Items(i)) and not Charlist.Items(i).isMember then
						perlimResult := perlimResult + 1;
				end;
				if (perlimResult > 5) then
					isPvpStatus := true
				else
					isPvpStatus := false;
			end;
			
			procedure PvPStuff;
			begin
				script.NewThread(@targetSaver);
				script.NewThread(@partyProtectThread);
				script.NewThread(@antiFear);
				script.NewThread(@buffDispel);
				script.NewThread(@newDispel);
				script.NewThread(@fearBuffsDispel);
				script.NewThread(@antiBackstab);
				script.NewThread(@antiHate);
				script.NewThread(@medusedTargetAvoid);
				script.NewThread(@holdTargetOly);
				delay(-1)
			end;
			
			procedure targetSaver;
			var
			Action : TL2Action;
			Prm1, Prm2: pointer;
			begin
			while Engine.Status = lsOnline do
			begin
				Action := engine.WaitAction([latarget], Prm1, Prm2);
				if Action = latarget then
					begin
					if (User.Target <> currentTarget) then
					begin
					previousTarget := currentTarget;
					currentTarget := User.Target;
					end;
					end;
			delay(50);
			end;
			end; 
			
			procedure partyProtectThread; {вместо User ставим PartyList.ByName('playerName')}
			var
				i: integer;
			begin
			while Engine.Status = lsOnline do
				begin
				for i:=0 to Charlist.Count - 1 do
				begin
				if shouldProtect(Charlist.Items(i), User)
				and shouldCast(Charlist.Items(i), 40, 900)
				then
					begin
						Engine.SetTarget(Charlist.Items(i));
						Engine.UseSkill(1436);
						if isTargetChanged(Charlist.Items(i), User) or Charlist.Items(i).Dead then
							begin
								Engine.SetTarget(previousTarget);
							end;
					end;
				end;
				delay(500);
				end;
			end;
			
			procedure antiFear;
			var
				i: integer;
				fearTemp: TL2Effect;
			begin
				while Engine.Status = lsOnline do
					begin
						for i:=0 to Charlist.Count - 1 do
							begin
								if isFearCasting(Charlist.Items(i)) and User.Running and not isAffraid(User) then
									begin
											Engine.UseAction(1);
											delay(fearCastEndTime + 200);
											if not User.Running and not User.Buffs.ByID(1169, fearTemp) then
												begin
													Engine.UseAction(1);
												end;
									end
								else if not User.Running and not isAffraid(User) then
									begin
										Engine.UseAction(1);
									end;
							end;
					delay(500);
					end;
			end;
			
			procedure buffDispel;
			var
				Buff: TL2Effect;
			begin
			if User.Buffs.ByID(buffID, Buff) and (fBuffCount >= 21) then
				begin
					Engine.Dispel(Buff.Name);
				end
			else 
				exit;
			end;
			
			procedure fearBuffsDispel;
			var
				fearTemp: TL2Effect;
			begin
				while Engine.Status = lsOnline do
					begin
						if User.Buffs.ByID(1169, fearTemp) then
							begin
								delay(randomDispersion(1000));
								buffDispel(1204); {windwalk}
								buffDispel(1282); {paagrio wind walk}
								buffDispel(1504); {imporved movement}
								buffDispel(1535); {chant of movement}
								buffDispel(2034); {Greater Haste Potion}						
							end;
					delay(500);
					end;
			end;
			
			procedure newDispel;
			var 
				buffTemp: TL2Effect;
			begin
				while Engine.Status = lsOnline do
					begin
						buffDispel(Regeneration);  		
						buffDispel(DeflectMagic);   	
						buffDispel(EvasKiss);   		
						buffDispel(Might);   			
						buffDispel(Focus);  			
						buffDispel(ChantOfEvision);   	
						buffDispel(Guidance);   		
						buffDispel(Invigor);   			
						buffDispel(Agility);  
						buffDispel(BlessedShield);   	
						buffDispel(AdvancedBlock);  
						buffDispel(ImpovedShieldDefence);   
						buffDispel(VampiricRage);   
						buffDispel(PoisonResistance);   
						buffDispel(Agility);   
						buffDispel(DecreaseWeigth);   							
						buffDispel(LordOfVampire);   							
						buffDispel(DeathWhisper);   							
						buffDispel(Haste);   							
						buffDispel(ForceMeditation);    				
						buffDispel(Invocation);   						
						buffDispel(PaagrioOfProtection);   				
						buffDispel(ChantOfRevenge);   					
						buffDispel(WarChant);   						
						buffDispel(GreaterMight);   					
						buffDispel(CriticalOfPaagrio);   				
						buffDispel(ChantOfCritical);   					
						buffDispel(ChantOfBloodAwakening);   			
						buffDispel(WeaponMaintenance);   				
						buffDispel(PaagriosEye);   						
						buffDispel(HolyWeapon);   						
						buffDispel(PaagriosVision);   					
						buffDispel(PaagriosHeart);   					
						buffDispel(PaagriosTact);   					
						buffDispel(ChantOfLife);   						
						buffDispel(GreaterHeal);   						
						buffDispel(GreaterGroupHeal);   			
						buffDispel(FuryOfPaagrio);   				
						buffDispel(WarCry);     						
						buffDispel(RapidShot);     					
						buffDispel(DuelistSpirit);    				
						buffDispel(IronShield);    					
						buffDispel(SpiritOfSagittarius);    		
						buffDispel(FastShot);    					
						buffDispel(Sharshooting);    				
						buffDispel(RapidFire);    					
						buffDispel(DeadEye);    					
						buffDispel(Snipe);    						
						buffDispel(HawkEye);    					
						buffDispel(FocusChance);    				
						buffDispel(FocusPower);    					
						buffDispel(FocusDeath);    					
						buffDispel(MortalStrike);    				
						buffDispel(ThrillFight);    				
						buffDispel(FellSwoop);    					
						buffDispel(DetectAnimalWeakness);     		
						buffDispel(DetectDragonWeakness);     		
						buffDispel(DetectBestWeakness);     		
						buffDispel(DetectInsectWeakness);     		
						buffDispel(DetectPlantWeakness);    		
						buffDispel(EyeOfHunter);    				
						buffDispel(EyeOfSlayer);    				
						buffDispel(CombatAura);    					
						buffDispel(SeedOfRevenge);    				
						buffDispel(SpiritOfPhoenix);    			
						buffDispel(EvasWill);    					
						buffDispel(PainOfShilen);    				
						buffDispel(Frenzy);    						
						buffDispel(Rage);     						
						buffDispel(ReflectDamage);     				
						buffDispel(DeflectArrow);    				
						buffDispel(PhysicalMirror);    				
						{buffDispel(351);}  						
						buffDispel(Majesty);     					
						buffDispel(DarkForm);    					
						buffDispel(SeedOfFire);   					
						buffDispel(SeedOfWater);   					
						buffDispel(SeedOfWind);   					
						buffDispel(FrostArmor);   					
						buffDispel(FlameArmor);
						buffDispel(HurricaneArmor);
						buffDispel(FireResistance);
						buffDispel(1182);
						buffDispel(1189);
						buffDispel(1548);
						buffDispel(1393);
						buffDispel(1392);
						buffDispel(1352);
						buffDispel(1549);
						buffDispel(1353);
						buffDispel(828);
						buffDispel(829);
						buffDispel(830);
						buffDispel(825);
						buffDispel(826);
						buffDispel(827);
						buffDispel(1307);
						buffDispel(1078);
						buffDispel(2103);
						buffDispel(2035);
						buffDispel(2235);
						buffDispel(2236);
						buffDispel(2237);
						buffDispel(2238);
						buffDispel(2239);
						buffDispel(2240);
						buffDispel(2012);
						buffDispel(2274);
						buffDispel(2335);
						buffDispel(2336);
						buffDispel(2337);
						buffDispel(2338);
						buffDispel(2339);
						buffDispel(2340);
						buffDispel(FuryOfPaagiro);
						buffDispel(MaximizeLongrangeWeaponUse);
						buffDispel(RecoverForce);
						buffDispel(MaximumRecovery);
						buffDispel(Smokescreen);
						buffDispel(MaximumResistStatus);
						buffDispel(MaximumDefense);
					delay(800);
					end;
			end;
			
			procedure antiBackstab;
			var
			i: integer;
			begin
				while Engine.Status = lsOnline do
				begin
					for i:=0 to Charlist.Count - 1 do
						begin
							if (Charlist.Items(i).Target=(User)) and (Charlist.Items(i).Cast.EndTime > 0) and
									((Charlist.Items(i).Cast.ID = 30) or
									(Charlist.Items(i).Cast.ID = 263) or
									(Charlist.Items(i).Cast.ID = 928) or
									(Charlist.Items(i).Cast.ID = 334)) and ShouldCast(Charlist.Items(i), 20, 700)
									then
							begin
								Engine.SetTarget(Charlist.Items(i));
								Engine.UseSkill(1394); {1436} //1394 = Trance
							end;
						end;
					delay(50);
				end;
			end;
		
		
			procedure medusedTargetAvoid;
			begin
				while Engine.Status = lsOnline do
					begin
						if isCasting and isMedused(User.Target) {and (enemyAround > 2)} then
							begin
								print('hey yo');
							end;
						delay(1000);	
					end;
			end;
		
			procedure antiHate;
			var
			i, iHater, iTargetBeforeHate: integer;
			Hater: TL2Live;
			TargetBeforeHate: TL2Live;
			temp: tl2effect;
			begin
			while Engine.Status = lsOnline do
			begin
				for i:=0 to Charlist.Count-1 do
				begin
				if ((Charlist.Items(i).Target = User) and (Charlist.Items(i).Cast.EndTime > 0) and
					((Charlist.Items(i).Cast.ID = 28) or (Charlist.Items(i).Cast.ID = 979))) then
					begin
					TargetBeforeHate:= User.Target;
					Engine.SetTarget(Charlist.Items(i));
					Hater:= Charlist.Items(i);
					iTargetBeforeHate:= 1;
					iHater := 1;
					Delay(Charlist.Items(i).Cast.EndTime + 50);
					end;
				if ((Charlist.Items(i).Cast.EndTime > 0) and (Charlist.Items(i).Attackable)and (Charlist.Items(i).Cast.ID = 18) and
					(User.DistTo(Charlist.Items(i)) < 400)) then
					begin
					TargetBeforeHate:= User.Target;
					Engine.SetTarget(Charlist.Items(i));
					Hater:= Charlist.Items(i);
					iTargetBeforeHate:= 1;
					iHater := 1;
					Delay(Charlist.Items(i).Cast.EndTime + 50);
					end;
				if ((iTargetBeforeHate > 0) and (User.Buffs.ByID(28, temp) or User.Buffs.ByID(979, temp) or User.Buffs.ByID(18, temp))
				and (User.Target = Hater)) then
					begin
						Engine.SetTarget(TargetBeforeHate);
						iTargetBeforeHate:= 0;
						iHater := 0;
					end;
				end;
				delay(200);
			end;
			end;
		
			procedure holdTargetOly;
			var
			prm1, prm2: pointer;
			Action : TL2Action;
			escBtn : boolean;
			begin
				while Engine.Status = lsOnline do
				begin
					Action := Engine.WaitAction([laUnTarget, laKey], prm1, prm2);
					if Action = laUnTarget then
					begin
						if not (User.Target = currentTarget) and not escBtn then
						begin
							delay(100);
							Engine.SetTarget(currentTarget); 
						end; 
						delay(100);
						escBtn := false;
					end;
					if Action = laKey then
					begin
						if (Integer(prm1) = $1B) then
							escBtn := true;
					end;
			end;
			end;
			
			{math functions}
			function isContains(arr: array of integer; val: integer): boolean;
			var
				i: integer;
			begin
				Result := false;
  
				if Length(arr) = 0 then
				   exit;

				for i:=0 to Length(arr)-1 do
				begin
				if (arr[i] = val) then
				begin
					Result := true;
					exit;
				end;
			end;
			end;
			
			{character status}
			function isStunned(obj: TL2Live): boolean;
			begin
				Result := obj.abnormalid = 64;
			end;
			
			function isTranced(obj: TL2Live): boolean;
			var
				buffsTemp: tl2effect;
			begin
				Result := obj.Buffs.ByID(1394, buffsTemp);
			end;
			
			function isAffraid(obj: TL2Live): boolean;
			var
				buffsTemp: tl2effect;
			begin
				Result := obj.Buffs.ByID(1169, buffsTemp) // curse fear
						or obj.Buffs.ByID(1092, buffsTemp) // wark fear
						or obj.Buffs.ByID(1381, buffsTemp) // mass fear
						or obj.Buffs.ByID(763, buffsTemp);  // heal scream
			end;
			
			function isMedused(obj: TL2Live): boolean;
			var
				buffsTemp: tl2effect;
			begin
				Result := obj.Buffs.ByID(367, buffsTemp); 
			end;
			
			function isAnchored(obj: TL2Live): boolean;
			var
				buffsTemp: tl2effect;
			begin
				Result := obj.Buffs.ByID(6092, buffsTemp) // Lightning Shock
						or obj.Buffs.ByID(5592, buffsTemp) // Lightning Barrier
						or obj.Buffs.ByID(6090, buffsTemp) // Lightning Strike
						or obj.Buffs.ByID(6091, buffsTemp) // Anchor
						or obj.Buffs.ByID(791, buffsTemp) // Lightning Shock 1st part
						or obj.Buffs.ByID(279, buffsTemp) // Lightning Strike 1st part
						or obj.Buffs.ByID(1170, buffsTemp); // Anchor 1st part
			end;
			
			function isRooted(obj: TL2Live): boolean;
			begin
				Result := obj.abnormalid = 512;
			end;
			
			function isDeathMark(obj: TL2Live): boolean;
			begin
				Result := obj.abnormalid = 1073741824;
			end;
			
			function isSilenced(obj: TL2Live): boolean;
			var
				buffsTemp: tl2effect;
			begin
			if obj.Buffs.ByID(1246, buffsTemp) // Seal of Silence
			or obj.Buffs.ByID(1064, buffsTemp) // Silence
			or obj.Buffs.ByID(437, buffsTemp) // Song of Silence
			or obj.Buffs.ByID(1336, buffsTemp) // Curse of Doom
			then
			isSilenced := true
			else
			isSilenced := false;
			end;
			
			function isRealTarget(obj: TL2Live): boolean;
			begin
				Result := obj.abnormalid = 536870912; 
			end;
			
			function isUltimateDefense(obj: TL2Live): boolean;
			begin
				Result := obj.abnormalid = 134217728;
			end;
			
			function isShadowed(obj: TL2Live): boolean;
			begin
				Result := obj.abnormalid = 1048576;
			end;
	
			//effs
			function isInvincible;
			var
				buffsTemp: TL2Effect;
			begin
				Result := obj.Buffs.ByID(1418, buffsTemp) or obj.Buffs.ByID(1427, buffsTemp) or
					obj.Buffs.ByID(1505, buffsTemp) or obj.Buffs.ByID(3158, buffsTemp) or
					obj.Buffs.ByID(655, buffsTemp) or obj.Buffs.ByID(5576, buffsTemp) or
					obj.Buffs.ByID(1540, buffsTemp) or obj.Buffs.ByID(837, buffsTemp);
			end;
			
			function isPvPdebuff;
			var
				buffsTemp: TL2Effect;
			begin
				Result := obj.Buffs.ByID(1248, buffsTemp) or obj.Buffs.ByID(1104, buffsTemp) or
					obj.Buffs.ByID(1247, buffsTemp) or obj.Buffs.ByID(1366, buffsTemp) or
					obj.Buffs.ByID(1263, buffsTemp) or obj.Buffs.ByID(1169, buffsTemp) or
					obj.Buffs.ByID(1369, buffsTemp) or obj.Buffs.ByID(1359, buffsTemp) or
					obj.Buffs.ByID(1337, buffsTemp) or obj.Buffs.ByID(1170, buffsTemp) or
					obj.Buffs.ByID(1360, buffsTemp);
			end;
			
			function isDisabled;
			var
				buffsTemp: TL2Effect;
			begin
				Result := isStunned(obj) or isMedused(obj) 
				or isTranced(obj) or isAnchored(obj) or isAffraid(obj) or
				obj.Buffs.ByID(1376, buffsTemp);
			end;
			
			function isSealOfLimit;
			var
				eff: TL2Buff;
			begin
				Result := obj.Buffs.ById(1509, eff) and (eff <> nil) and (eff.endtime > 480);
			end;
				
			function isOverweight;
			begin
				Result := (User.Load >= limit);
			end;
			
			function randomDispersion;
			var
				min, max: integer;
			begin
				min:= Trunc(x * 0.8);
				max:= Trunc(x * 1.2);
				randomDispersion := Random(max - min + 1) + min;  
			end;
			
			function fNotBuff;
			begin
				if ((Pos('Dance',  sName) <> 0) or
				(Pos('Dance' ,sName) <> 0) or
				(Pos('Song' ,sName) <> 0) or
				(Pos('Seal' ,sName) <> 0) or
				(Pos('Block' ,sName) <> 0) or
				((Pos('Lightning' ,sName) <> 0) and not (Pos('Barrier' ,sName) <> 0)) or
				(Pos('Vortex' ,sName) <> 0) or
				(Pos('Surrender' ,sName) <> 0) or
				(Pos('Curse' ,sName) <> 0) or
				((Pos('Symphony' ,sName) <> 0) and not (Pos('Noblesse' ,sName) <> 0)) or
				((Pos('Decrease' ,sName) <> 0) and not (Pos('Weight' ,sName) <> 0)) or
				((Pos('Poison' ,sName) <> 0) and not (Pos('Resist' ,sName) <> 0)) or
				((Pos('Shock' ,sName) <> 0) and not (Pos('Resist' ,sName) <> 0)) or
				((Pos('Freezing' ,sName) <> 0) and not (Pos('Skin' ,sName) <> 0)) or
				((Pos('Arcane' ,sName) <> 0) and not (Pos('Protection' ,sName) <> 0)) or
				(Pos('Transform' ,sName) <> 0) or
				(Pos('Shackle' ,sName) <> 0) or
				(Pos('Mass' ,sName) <> 0) or
				(Pos('Rush' ,sName) <> 0) or
				(Pos('Death' ,sName) <> 0) or
				(Pos('Stun' ,sName) <> 0) or
				(Pos('Root' ,sName) <> 0) or
				(Pos('Hold' ,sName) <> 0) or
				(Pos('Paraly' ,sName) <> 0) or
				(Pos('Doom' ,sName) <> 0) or
				(Pos('Hot Springs' ,sName) <> 0) or
				(Pos('Silence' ,sName) <> 0) or
				(Pos('Sleep' ,sName) <> 0) or
				(Pos('Trance' ,sName) <> 0) or
				(Pos('Fear' ,sName) <> 0) or
				(Pos('Terror' ,sName) <> 0) or
				(Pos('Horror' ,sName) <> 0) or
				(Pos('Slow' ,sName) <> 0) or
				(Pos('Anchor' ,sName) <> 0) or
				(Pos('Anger' ,sName) <> 0) or
				(Pos('Bleed' ,sName) <> 0) or
				(Pos('Stigma' ,sName) <> 0) or
				(Pos('Turn to Stone' ,sName) <> 0) or
				(Pos('Magical Backfire' ,sName) <> 0) or
				(Pos('Heroic Grandeur' ,sName) <> 0) or
				(Pos('Dread' ,sName) <> 0) or
				(Pos('Disarm' ,sName) <> 0) or
				(Pos('Real Target' ,sName) <> 0) or
				(Pos('Critical Wound' ,sName) <> 0) or
				(Pos('Arrest' ,sName) <> 0) or
				(Pos('Hate' ,sName) <> 0) or
				(Pos('Aggression' ,sName) <> 0) or
				(Pos('Burning Chop' ,sName) <> 0) or
				(Pos('Ignore Shield Defense' ,sName) <> 0) or
				(Pos('Speed Down' ,sName) <> 0) or
				(Pos('Violent Temper' ,sName) <> 0) or
				(Pos('Shield Bash' ,sName) <> 0) or
				(Pos('Shield Slam' ,sName) <> 0) or
				(Pos('Earthquake' ,sName) <> 0) or
				(Pos('Cripple' ,sName) <> 0) or
				(Pos('Entangle' ,sName) <> 0) or
				(Pos('Hex' ,sName) <> 0) or
				(Pos('Break' ,sName) <> 0) or
				(Pos('Hamstring' ,sName) <> 0) or
				(Pos('Sting' ,sName) <> 0) or
				(Pos('Bluff' ,sName) <> 0) or
				(Pos('Tribunal' ,sName) <> 0) or
				(Pos('Judgment' ,sName) <> 0) or
				(Pos('Sand Bomb' ,sName) <> 0) or
				(Pos('Weapon Blockade' ,sName) <> 0) or
				(Pos('Onslaught of Pa''agrio' ,sName) <> 0) or
				(Pos('Aura Sink' ,sName) <> 0) or
				(Pos('Frost' ,sName) <> 0) or
				(Pos('Ice Dagger' ,sName) <> 0) or
				(Pos('Blizzard' ,sName) <> 0) or
				(Pos('Demon Wind' ,sName) <> 0) or
				(Pos('Decay' ,sName) <> 0) or
				(Pos('Diamond Dust' ,sName) <> 0) or
				(Pos('Throne' ,sName) <> 0) or
				(Pos('Count of Fire' ,sName) <> 0) or
				(Pos('Vampiric Mist' ,sName) <> 0) or
				(Pos('Bind' ,sName) <> 0) or
				(Pos('Blink' ,sName) <> 0) or
				(Pos('Flame Armor' ,sName) <> 0) or
				(Pos('Hurricane Armor' ,sName) <> 0) or
				(Pos('Dreaming Spirit' ,sName) <> 0) or
				(Pos('Inferno' ,sName) <> 0) or
				(Pos('Antharas' ,sName) <> 0) or
				(Pos('Heat of Desert' ,sName) <> 0) or
				(Pos('Inquisitor' ,sName) <> 0) or
				(Pos('Charm of Courage' ,sName) <> 0) or
				(Pos('Protection Power' ,sName) <> 0) or
				(Pos('Enchanter Ability' ,sName) <> 0) or
				(Pos('Knight Ability' ,sName) <> 0) or
				(Pos('PvP Armor - Critical Down' ,sName) <> 0) or
				(Pos('PvP Weapon - Casting' ,sName) <> 0) or
				(Pos('Great Fury' ,sName) <> 0) or
				(Pos('Maximum Ability' ,sName) <> 0) or
				(Pos('Counter Rapid Shot' ,sName) <> 0) or
				(Pos('Counter Dash' ,sName) <> 0) or
				(Pos('Totem''s Energy',sName) <> 0) or
				(Pos('Morale Boost' ,sName) <> 0)) then
					fNotBuff := true
				else
					fNotBuff := false;
			end;
			
			procedure fNotBuffCount;
			var 
				i: integer;
				perlimResult : integer;
			begin
				while Engine.Status = lsOnline do
				begin
					perlimResult := 0;
					for i:=0 to User.Buffs.Count-1 do
					begin
						if fNotBuff(User.Buffs.Items(i).Name) then
							perlimResult := perlimResult + 1;
						if (User.Buffs.Items(i).ID = 6059) then
							perlimResult := perlimResult + 1;
						if (User.Buffs.Items(i).ID = 4268) then
							perlimResult := perlimResult + 1;
					end;
					vNotBuffCount := perlimResult;
					delay(500);
				end;
			end;
			
			function fBuffCount;
			begin
				Result := User.Buffs.Count - vNotBuffCount; 
			end;
			
			{function isCasting;
			begin
				Result := (obj.Cast.EndTime > 0);
			end;}
			
			//relations
			function isFriend;
			begin
				Result := victim.Attackable
				and (
				not victim.pvp
				or (victim.pvp and victim.isMember)
				or (victim.pvp and isClanMember(victim))
				);
			end;
			
			function isClanMember;
			begin
				Result := (User.ClanID = victim.ClanID);
			end;
			
			function isAllyMember;
			begin
				Result := (User.AllyID = victim.AllyID);
			end;
			
			{class defination}
			
			{buffs status}
			
			{class status}
end.